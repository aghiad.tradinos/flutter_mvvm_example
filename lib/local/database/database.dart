import 'dart:async';
import 'package:floor/floor.dart';
import 'package:flutter_mvvm_example/local/converters/user_converter.dart';
import 'package:flutter_mvvm_example/local/dao/user_dao.dart';
import 'package:flutter_mvvm_example/models/data/user/user.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import '../entity/user_entity.dart';

part 'database.g.dart'; // the generated code will be there

@TypeConverters([UserConverter])
@Database(version: 1, entities: [UserEntity])
abstract class AppDatabase extends FloorDatabase {
  UserDao get userDao;
}
