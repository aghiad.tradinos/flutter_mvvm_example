import 'package:floor/floor.dart';
import 'package:flutter_mvvm_example/models/data/user/user.dart';

@entity
class UserEntity {
  @PrimaryKey(autoGenerate: false)
  final String id;
  final int page;
  final User user;

  const UserEntity({required this.id, required this.page, required this.user});

  @override
  String toString() {
    return user.toJson().toString();
  }
}
