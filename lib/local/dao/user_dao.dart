import 'package:floor/floor.dart';
import 'package:flutter_mvvm_example/models/data/user/user.dart';

import '../entity/user_entity.dart';

@dao
abstract class UserDao {
  @Query('SELECT * FROM UserEntity')
  Future<List<UserEntity>> findAllUsers();

  @Query('SELECT * FROM UserEntity WHERE id = :id')
  Future<UserEntity?> findUserById(String id);

  @Query('SELECT * FROM UserEntity WHERE page = :page')
  Future<List<UserEntity>?> findUsersByPage(int page);

  @Query('DELETE FROM UserEntity WHERE page = :page')
  Future<List<UserEntity>?> removeUsersByPage(int page);

  @Query('DELETE FROM UserEntity')
  Future<List<UserEntity>?> removeAllUsers();

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<void> insertUser(UserEntity user);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<void> insertUsers(List<UserEntity> users);

  @transaction
  Future<void> refreshUsers(List<User> users, int page) async {
    await removeUsersByPage(page);
    final list = users.map((e) => mappingToEntity(e, page)).toList();
    await insertUsers(list);
  }

  UserEntity mappingToEntity(User user, int page) {
    return UserEntity(id: user.id.toString(), user: user, page: page);
  }
}
