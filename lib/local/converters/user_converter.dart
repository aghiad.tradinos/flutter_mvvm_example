import 'dart:convert';
import 'package:floor/floor.dart';
import '../../models/data/user/user.dart';

class UserConverter extends TypeConverter<User, String> {
  @override
  User decode(String databaseValue) {
    final map = json.decode(databaseValue);
    return User.fromJson(map);
  }

  @override
  String encode(User value) {
    return json.encode(value);
  }
}
