import 'package:flutter/material.dart';
import 'package:flutter_mvvm_example/models/data/user/user.dart';
import 'package:flutter_mvvm_example/ui/resources/dimensions/dimensions.dart';
import 'package:flutter_mvvm_example/ui/resources/themes/themes.dart';
import 'package:flutter_mvvm_example/ui/widgets/text_view.dart';

class UserCard extends StatelessWidget {
  final User user;
  const UserCard({required this.user, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: cardDecoration(Theme.of(context)),
      margin: const EdgeInsets.symmetric(vertical: halfUnit, horizontal: 2),
      padding: const EdgeInsets.all(halfUnit),
      child: Row(
        children: [
          if (user.avatar != null)
            ClipOval(
              child: Image.network(
                user.avatar!,
                width: 60,
                height: 60,
                fit: BoxFit.cover,
              ),
            ),
          const SizedBox(width: unit),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("${user.id}. ${user.name}", style: mediumTextStyle),
              const SizedBox(height: unit),
              Text(user.company ?? "", style: smallTextStyle),
              const SizedBox(height: unit),
              TextView(
                label: "${user.country}, ${user.city}",
                style: verySmallTextStyle,
                widthPercentage: 0.6,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
