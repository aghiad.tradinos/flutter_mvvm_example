import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mvvm_example/ui/core/providers/theme_provider.dart';
import 'package:flutter_mvvm_example/ui/resources/dimensions/dimensions.dart';
import 'package:provider/provider.dart';

class ThemeSwitchWidget extends StatelessWidget {
  const ThemeSwitchWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ThemeProvider>(context);
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: towUnits),
      child: Row(
        children: [
          Text("dark_mode".tr()),
          Switch(
            value: provider.mode == ThemeMode.dark,
            onChanged: (value) {
              provider.setDarkTheme(value);
            },
          ),
        ],
      ),
    );
  }
}
