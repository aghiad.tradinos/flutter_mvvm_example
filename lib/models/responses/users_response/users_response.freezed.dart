// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'users_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UsersResponse _$UsersResponseFromJson(Map<String, dynamic> json) {
  return _UsersResponse.fromJson(json);
}

/// @nodoc
class _$UsersResponseTearOff {
  const _$UsersResponseTearOff();

  _UsersResponse call({List<User?>? users, int? total}) {
    return _UsersResponse(
      users: users,
      total: total,
    );
  }

  UsersResponse fromJson(Map<String, Object?> json) {
    return UsersResponse.fromJson(json);
  }
}

/// @nodoc
const $UsersResponse = _$UsersResponseTearOff();

/// @nodoc
mixin _$UsersResponse {
  List<User?>? get users => throw _privateConstructorUsedError;
  int? get total => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UsersResponseCopyWith<UsersResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UsersResponseCopyWith<$Res> {
  factory $UsersResponseCopyWith(
          UsersResponse value, $Res Function(UsersResponse) then) =
      _$UsersResponseCopyWithImpl<$Res>;
  $Res call({List<User?>? users, int? total});
}

/// @nodoc
class _$UsersResponseCopyWithImpl<$Res>
    implements $UsersResponseCopyWith<$Res> {
  _$UsersResponseCopyWithImpl(this._value, this._then);

  final UsersResponse _value;
  // ignore: unused_field
  final $Res Function(UsersResponse) _then;

  @override
  $Res call({
    Object? users = freezed,
    Object? total = freezed,
  }) {
    return _then(_value.copyWith(
      users: users == freezed
          ? _value.users
          : users // ignore: cast_nullable_to_non_nullable
              as List<User?>?,
      total: total == freezed
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc
abstract class _$UsersResponseCopyWith<$Res>
    implements $UsersResponseCopyWith<$Res> {
  factory _$UsersResponseCopyWith(
          _UsersResponse value, $Res Function(_UsersResponse) then) =
      __$UsersResponseCopyWithImpl<$Res>;
  @override
  $Res call({List<User?>? users, int? total});
}

/// @nodoc
class __$UsersResponseCopyWithImpl<$Res>
    extends _$UsersResponseCopyWithImpl<$Res>
    implements _$UsersResponseCopyWith<$Res> {
  __$UsersResponseCopyWithImpl(
      _UsersResponse _value, $Res Function(_UsersResponse) _then)
      : super(_value, (v) => _then(v as _UsersResponse));

  @override
  _UsersResponse get _value => super._value as _UsersResponse;

  @override
  $Res call({
    Object? users = freezed,
    Object? total = freezed,
  }) {
    return _then(_UsersResponse(
      users: users == freezed
          ? _value.users
          : users // ignore: cast_nullable_to_non_nullable
              as List<User?>?,
      total: total == freezed
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_UsersResponse implements _UsersResponse {
  const _$_UsersResponse({this.users, this.total});

  factory _$_UsersResponse.fromJson(Map<String, dynamic> json) =>
      _$$_UsersResponseFromJson(json);

  @override
  final List<User?>? users;
  @override
  final int? total;

  @override
  String toString() {
    return 'UsersResponse(users: $users, total: $total)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _UsersResponse &&
            const DeepCollectionEquality().equals(other.users, users) &&
            const DeepCollectionEquality().equals(other.total, total));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(users),
      const DeepCollectionEquality().hash(total));

  @JsonKey(ignore: true)
  @override
  _$UsersResponseCopyWith<_UsersResponse> get copyWith =>
      __$UsersResponseCopyWithImpl<_UsersResponse>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UsersResponseToJson(this);
  }
}

abstract class _UsersResponse implements UsersResponse {
  const factory _UsersResponse({List<User?>? users, int? total}) =
      _$_UsersResponse;

  factory _UsersResponse.fromJson(Map<String, dynamic> json) =
      _$_UsersResponse.fromJson;

  @override
  List<User?>? get users;
  @override
  int? get total;
  @override
  @JsonKey(ignore: true)
  _$UsersResponseCopyWith<_UsersResponse> get copyWith =>
      throw _privateConstructorUsedError;
}
