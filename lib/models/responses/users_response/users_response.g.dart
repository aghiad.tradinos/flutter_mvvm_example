// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'users_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UsersResponse _$$_UsersResponseFromJson(Map json) => _$_UsersResponse(
      users: (json['users'] as List<dynamic>?)
          ?.map((e) => e == null
              ? null
              : User.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
      total: json['total'] as int?,
    );

Map<String, dynamic> _$$_UsersResponseToJson(_$_UsersResponse instance) =>
    <String, dynamic>{
      'users': instance.users?.map((e) => e?.toJson()).toList(),
      'total': instance.total,
    };
