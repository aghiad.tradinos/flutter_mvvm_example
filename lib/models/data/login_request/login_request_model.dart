import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_request_model.freezed.dart';
part 'login_request_model.g.dart';

@freezed
class LoginRequest with _$LoginRequest {
  @JsonSerializable(explicitToJson: true)
  const factory LoginRequest(
    String email,
    String password,
  ) = _LoginRequest;

  factory LoginRequest.fromJson(Map<String, dynamic> json) => _$LoginRequestFromJson(json);
}
