import 'dart:async';
import 'package:animated_infinite_scroll_pagination/animated_infinite_scroll_pagination.dart';
import 'package:dio/dio.dart';
import 'package:flutter_mvvm_example/extensions/nullable_extensions.dart';
import 'package:flutter_mvvm_example/local/dao/user_dao.dart';
import 'package:flutter_mvvm_example/models/data_models.dart';
import 'package:get_it/get_it.dart';
import 'package:lazy_evaluation/lazy_evaluation.dart';
import '../config/singleton_config.dart';
import '../local/database/database.dart';
import '../models/data/login_request/login_request_model.dart';
import '../models/responses/base_response/base_response.dart';
import 'base_repository.dart';
import '../network/user_rest_client/user_rest_client.dart';

class UserRepository extends BaseRepository {
  final _userRestClient = Lazy<UserRestClient>(() => UserRestClient(GetIt.I.get<Dio>()));
  UserRestClient get userRestClient => _userRestClient.value;

  final _appDatabase = Lazy<AppDatabase>(() => getSingleton<AppDatabase>());
  UserDao get userDao => _appDatabase.value.userDao;

  final _controller = StreamController<PaginationState<List<User>>>();

  Stream<PaginationState<List<User>>> get result async* {
    yield* _controller.stream;
  }

  Future<BaseResponse<LoginResponse>> login(LoginRequest loginRequest) async {
    return getResponse(() => userRestClient.login(loginRequest).onError((error, _) => catchError<LoginResponse>(error)));
  }

  Future<int> getUsersList(int page) async {
    // get cached data
    final cachedData = await userDao.findUsersByPage(page);
    if (cachedData.orEmpty().isNotEmpty) {
      final paragraphs = cachedData.orEmpty().map((e) => e.user).toList();
      _controller.add(PaginationSuccess(paragraphs, cached: true));
    }

    // fetch data from server
    final response = await getResponse(() => userRestClient.getUsersList(page: page).onError((error, _) => catchError<UsersResponse>(error)));
    if (response.success) {
      final users = response.data!.users.orEmpty();
      _controller.add(PaginationSuccess(users));

      // insert data to local storage
      userDao.refreshUsers(users, page);

      return response.data?.total ?? 0;
    } else {
      _controller.add(const PaginationError());
      return 0;
    }
  }
}
