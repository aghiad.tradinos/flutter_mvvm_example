import 'package:dio/dio.dart';
import 'package:flutter_mvvm_example/models/data_models.dart';
import 'package:retrofit/retrofit.dart';
import '../../models/data/login_request/login_request_model.dart';
import '../../models/responses/base_response/base_response.dart';
import '../config/env.dart';
part 'user_rest_client.g.dart';

@RestApi(baseUrl: Env.apiUrl)
abstract class UserRestClient {
  factory UserRestClient(Dio dio) = _UserRestClient;

  @POST("/login")
  Future<BaseResponse<LoginResponse>> login(@Body() LoginRequest loginRequest);

  @GET("/users")
  Future<BaseResponse<UsersResponse>> getUsersList({
    @Query("page") required int page,
    @Query("limit") int perPage = Env.perPage,
  });
}
