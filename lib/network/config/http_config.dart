import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import '../../../ui/core/events/bus_events.dart';
import '../managers/authentication_manager.dart';
import 'logging_interceptor.dart';

/// Config Http Options
Future<void> httpConfig() async {
  await _httpClientConfig();
}

/// Config Http globally
///
/// Add "Authorization" in header to every request sent,
///
/// Add Log on console for see (Request/Response) details.
_httpClientConfig() {
  GetIt.I.registerSingleton<Dio>(Dio()
    ..interceptors.add(
      InterceptorsWrapper(
        onRequest: (request, handler) async {
          var headers = {
            'Accept': 'application/json',
            'format': 'json',
            'Authorization': 'Bearer ${await AuthenticationManager.token()}',
          };
          request.headers.addAll(headers);
          request.connectTimeout = 100000;
          request.receiveTimeout = 100000;
          handler.next(request);
        },
        onError: (error, handler) async {
          if (error.response?.statusCode == 401 || error.response?.statusCode == 403) {
            // logOut from main.dart
            eventBus.fire(const UnauthorizedEvent());
          }
          handler.next(error);
        },
        onResponse: (response, handler) {
          handler.next(response);
        },
      ),
    )
    ..interceptors.add(LoggingInterceptor()));
}
