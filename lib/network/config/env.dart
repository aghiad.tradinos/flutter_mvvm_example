class Env {
  static const localhost = "http://192.168.1.8:3000";
  static const stagingUrl = "https://xdevelop.herokuapp.com";
  static const liveUrl = "https://www.domain.com";

  static const baseUrl = stagingUrl;

  static const apiUrl = "$baseUrl/dummy";

  /// pagination
  static const perPage = 15;
}
