import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class LoggingInterceptor extends Interceptor {
  @override
  FutureOr<dynamic> onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    if (kDebugMode) {
      print('REQUEST');
    }
    if (kDebugMode) {
      print("--> ${options.method.toUpperCase()} ${(options.baseUrl) + (options.path)}");
    }
    if (kDebugMode) {
      print("Headers:");
    }
    options.headers.forEach((k, v) {
      if (kDebugMode) {
        print('$k: $v');
      }
    });

    if (kDebugMode) {
      print("queryParameters:");
    }
    options.queryParameters.forEach((k, v) {
      if (kDebugMode) {
        print('$k: $v');
      }
    });

    if (options.data != null) {
      if (kDebugMode) {
        print("Body: ${options.data}");
      }
    }
    if (kDebugMode) {
      print("--> END ${options.method.toUpperCase()}");
    }

    return handler.next(options);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    if (kDebugMode) {
      print('ERROR');
    }
    if (kDebugMode) {
      print(
          "<-- ${err.message} ${(err.response?.requestOptions != null ? (err.response!.requestOptions.baseUrl + err.response!.requestOptions.path) : 'URL')}");
    }
    if (kDebugMode) {
      print("${err.response != null ? err.response?.data : 'Unknown Error'}");
    }
    if (kDebugMode) {
      print("<-- End error");
    }
    return handler.next(err);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    if (kDebugMode) {
      print('RESPONSE');
    }
    if (kDebugMode) {
      print("<-- ${response.statusCode} ${((response.requestOptions.baseUrl + response.requestOptions.path))}");
    }
    if (kDebugMode) {
      print("Headers:");
    }
    response.headers.forEach((k, v) {
      if (kDebugMode) {
        print('$k: $v');
      }
    });
    if (kDebugMode) {
      print("Response: ${response.data}");
    }
    if (kDebugMode) {
      print("<-- END HTTP\n-------------------------------------------");
    }
    return handler.next(response);
  }
}
